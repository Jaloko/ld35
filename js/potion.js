var Potions = {
	HP: 0,
	MAX_HP: 1,
	INVINCIBLE: 2,
	SPD: 3

}

var Potion = function(type, x ,y) {
	this.type = type,
	this.pos = {
		x: x,
		y: y,
	},
	this.width = type == Potions.SPD ? 16 : 9,
	this.height = 12,
	this.flagForDeletion = false
}

Potion.prototype.update = function() {
	this.checkCollision();
}

Potion.prototype.checkCollision = function() {
	if(this.pos.x + this.width * RENDER_SCALE_MULTIPLIER / 2 >= Player.pos.x 
		&& this.pos.x + this.width * RENDER_SCALE_MULTIPLIER / 2 <= Player.pos.x + (Player.width * RENDER_SCALE_MULTIPLIER) &&
		this.pos.y + this.height * RENDER_SCALE_MULTIPLIER / 2 >= Player.pos.y 
		&& this.pos.y + this.height * RENDER_SCALE_MULTIPLIER / 2 <= Player.pos.y + (Player.height * RENDER_SCALE_MULTIPLIER)) {
		switch(this.type) {
			case Potions.HP:
				Player.health += 3;
				if(Player.health > Player.maxHealth) {
					Player.health = Player.maxHealth;
				}				
				break;
			case Potions.MAX_HP:
				Player.health = Player.maxHealth;
				break;
			case Potions.INVINCIBLE:
				Player.invincibleTimer = new Date().getTime();
				break;
			case Potions.SPD:
				Player.speedBoost = 3;
				Player.speedBoostTimer = new Date().getTime();
				break;
		}
		AudioManager.powerup.play();
		this.flagForDeletion = true;

	}
}

Potion.prototype.render = function(ctx) {
	var pos = Utils.getMobDrawPosition(this.pos.x, this.pos.y);
	var p = ImageLoader.images["potions"];
	ctx.drawImage(p, this.type * 9, 0, this.width, this.height,
		pos.x, pos.y, this.width * RENDER_SCALE_MULTIPLIER, this.height * RENDER_SCALE_MULTIPLIER);
};