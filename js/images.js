var ImageLoader = {
    filePaths: {
        "tiles": "img/tiles.png",
        "player": "img/player.png",
        "player-spritesheet": "img/player-spritesheet.png",
        "weapon": "img/test-weapon.png",
        "weapons": "img/weapons.png",
        "mob": "img/mobs.png",
        "smoke": "img/smoke.png",
        "bg": "img/bg.png",
        "potions": "img/potions.png"
    },
    images: {}
}

ImageLoader.loadImages = function(next) {
    var imageLoadedCount = 0;
    var t = this;
    
    for (var k in this.filePaths) {
        this.images[k] = new Image();
        
        this.images[k].onload = function() {
            imageLoadedCount++;
            if (imageLoadedCount == Object.keys(t.filePaths).length) {
                next();
            }
        }
        
        this.images[k].src = this.filePaths[k];
    }
}