var SoundBank = function(audioURL, amount) {
	this.items = [];
	// Multiple instances so that sounds can play at the same time
	// if one hasnt finished playing yet
	for(var i = 0; i < amount; i++){
		this.items.push(new Audio(audioURL));
	}
}

SoundBank.prototype = {
	set volume(val) {
		for(var i = 0; i < this.items.length; i++) {
			this.items[i].volume = val;
		}
	}
}

SoundBank.prototype.play = function() {
	for(var i = 0; i < this.items.length; i++) {
		if(this.items[i].duration > 0 && !this.items[i].paused) {

		} else {
			this.items[i].play();
			break;
		}
	}
}

var AudioManager = { 
	ctx: null,
	frequencyData: null,
	analyser: null,
	meleeHit: null,
	arrowHit: null,
	jump: null,
	powerup: null

};

AudioManager.init = function() {
	this.meleeHit = new SoundBank("audio/meleehit.wav", 5);
	this.arrowHit = new SoundBank("audio/arrowhit.wav", 5);
	this.jump = new SoundBank("audio/jump.wav", 2);
	this.powerup = new SoundBank("audio/powerup.wav", 2);

	// Init sound effects
	this.meleeHit.volume = 0.2;
	this.arrowHit.volume = 0.2;
	this.jump.volume = 0.2;
	this.powerup.volume = 0.2;

	this.ctx = new AudioContext();
	var audio = document.getElementById("audio");
	audio.volume = 0.2;
	var audioSrc = this.ctx.createMediaElementSource(audio);
	this.analyser = this.ctx.createAnalyser();
	// we have to connect the MediaElementSource with the analyser 
	audioSrc.connect(this.analyser);
	audioSrc.connect(this.ctx.destination);
	// we could configure the analyser: e.g. analyser.fftSize (for further infos read the spec)

	// frequencyBinCount tells you how many values you'll receive from the analyser
	this.frequencyData = new Uint8Array(this.analyser.frequencyBinCount);
}

AudioManager.stop = function(){
	var sound =document.getElementById('audio');
    sound.pause();
    sound.currentTime = 0;
}

AudioManager.update = function() {
	this.analyser.getByteFrequencyData(this.frequencyData);
	var audio = document.getElementById("audio");
	//console.log(audio.volume);
}

AudioManager.changeVolume = function(val) {
	var audio = document.getElementById("audio");
	audio.volume = val;
	this.meleeHit.volume = val;
	this.arrowHit.volume = val;
	this.jump.volume = val;
	this.powerup.volume = val;
	var icon = 	document.getElementById("volume-icon");
	if(val <= 0) {
		icon.style.backgroundImage = "url('img/volume-mute.png')";
	}
	else if(val > 0 && val <= 0.5) {
		icon.style.backgroundImage = "url('img/volume-down.png')";
	} else if(val > 0.5) {
		icon.style.backgroundImage = "url('img/volume-up.png')";
	}

}