var Projectile = function(x, y, dir) {
	this.pos = {
		x: x,
		y: y
	},
	this.width = 21,
	this.height = 7,
	this.speed = 9,
	// True is left, false is right
	this.dir = dir,
	this.flagForDeletion = false
}

Projectile.prototype.getCenter = function() {
	return { x: this.pos.x + this.width * RENDER_SCALE_MULTIPLIER / 2, y: this.pos.y + this.height * RENDER_SCALE_MULTIPLIER /2 };
}

Projectile.prototype.update = function() {
	this.checkCollision();
	if(this.dir) {
		for(var i = 0; i < this.speed; i++) {
			this.pos.x--;
		}

	} else {
		for(var i = 0; i < this.speed; i++) {
			this.pos.x++;
		}
	}
}

Projectile.prototype.checkCollision = function() {
	var center = this.getCenter();
	for(var i = 0; i < mobs.length; i++) {
		if(center.x >= mobs[i].pos.x && center.x <= mobs[i].pos.x + mobs[i].width &&
			center.y >= mobs[i].pos.y && center.y <= mobs[i].pos.y + mobs[i].height) {
				mobs[i].alterHealth(1);
				AudioManager.arrowHit.play();
				this.flagForDeletion = true;
				break;
		} else if(LEVEL_DATA[Math.floor(center.y / 32)][Math.floor(center.x / 32)] != 0) {
			this.flagForDeletion = true;
		}
	}


}

Projectile.prototype.render = function(ctx) {
	var weapons = ImageLoader.images["weapons"];
	var pos = Utils.getMobDrawPosition(this.pos.x, this.pos.y);
	var mob = ImageLoader.images["mob"];

	if(this.dir) {
		ctx.drawImage(weapons, 38, 102, this.width, this.height,
			pos.x, pos.y, this.width * RENDER_SCALE_MULTIPLIER, this.height * RENDER_SCALE_MULTIPLIER);

	} else {
		ctx.drawImage(weapons, 38, 96, this.width, this.height,
			pos.x, pos.y, this.width * RENDER_SCALE_MULTIPLIER, this.height * RENDER_SCALE_MULTIPLIER);
	}	
}