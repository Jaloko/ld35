/*
	Soz about the utter mess in here - Cory
	Notes:
	Collision works but is off at the moment because of the new sprite size, plz fix
*/

var Weapons = {
	NONE: 0,
	SWORD: 1,
	SPEAR: 2,
	AXE: 3,
	BOW: 4,
	SHIELD: 5
}

var Player = { 
	width: 22,
	height: 30,
};



Player.init = function() {
	// End of map
	//Object {x: 5880, y: 1027}
	// Player initial state
	this.pos = {x: 1*32, y: 22*32 + 3};
	this.speed = 3;
	this.speedBoost = 0;
	this.speedBoostTimer = 0;
	this.speedBoostTime = 10;
	this.invincibleTimer = 0;
	this.invincibleTime = 15;
	this.health = 10;
	this.maxHealth = 10;
	this.moving = false;
	this.movingLeft = false;
	this.playerIsLeftClick = false;
	this.jumpPressed = false;
	this.jump = false;
	this.jumpCount = 0;
	this.weaponChanged = false;
	this.endGame = false;
	this.attackTimer = new Date().getTime();
	this.jumpTimer = new Date().getTime();
	this.jumpOrigPos = {x: 0, y: 0};
	this.gravity = 3;
	this.equipedWeapon = Weapons.SWORD;
	this.currentLeftAnim = null;
	this.currentRightAnim = null;
	this.animations = Array(7);

	this.smoke = new Animation({
		frames: ImageLoader.images["smoke"], 
		width: 23,
		height: 18,
		frameXStart: -1,
		frameYStart: 0,
		maxFrames: 1
	});
	
	// Init player animations
	this.animations[0] = new Animation({
		frames: ImageLoader.images["player-spritesheet"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 1,
		maxFrames: 5
	});

	this.animations[1] = new Animation({
		frames: ImageLoader.images["player-spritesheet"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 0,
		maxFrames: 5
	});
	this.animations[2] = new Animation({
		frames: ImageLoader.images["player-spritesheet"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 3,
		maxFrames: 5
	});

	this.animations[3]  = new Animation({
		frames: ImageLoader.images["player-spritesheet"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 2,
		maxFrames: 5
	});
	this.animations[4]  = new Animation({
		frames: ImageLoader.images["player-spritesheet"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 5,
		maxFrames: 5
	});

	this.animations[5]  = new Animation({
		frames: ImageLoader.images["player-spritesheet"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 4,
		maxFrames: 5
	});
	this.animations[6] = new Animation({
		frames: ImageLoader.images["player-spritesheet"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 7,
		maxFrames: 5
	});

	this.animations[7] = new Animation({
		frames: ImageLoader.images["player-spritesheet"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 6,
		maxFrames: 5
	});

	this.changeWeapon();
	this.setWeapon(Weapons.SWORD);

	ctx.canvas.addEventListener('mousedown', Player.onMouseDown, false);
	ctx.canvas.addEventListener('mouseup', Player.onMouseUp, false);
	ctx.canvas.addEventListener('click', Player.onMouseClick, false);
}

Player.onMouseDown = function(e) {
	if(e.button === 0){
		Player.isLeftClick = true;
	}
}

Player.onMouseUp = function(e) {
	if(e.button === 0){
		Player.isLeftClick = false;
	}
}

Player.onMouseClick = function(e) {
	if(e.button === 0){
		Player.attack();
	}
}

//TODO: where does this belong?
var RENDER_SCALE_MULTIPLIER = 2;


Player.image = new Image();

Player.changeWeapon = function(){
	var weapon = AudioManager.frequencyData[1];

	if(weapon <= 63){
		if(this.equipedWeapon != Weapons.SWORD) {
			this.weaponChanged = true;
			this.setWeapon(Weapons.SWORD);
		}
	}
	else if(weapon > 63 && weapon <= 126){
		if(this.equipedWeapon != Weapons.SPEAR) {
			this.weaponChanged = true;
			this.setWeapon(Weapons.SPEAR);
		}
	}

	else if(weapon > 126 && weapon <= 189){
		if(this.equipedWeapon != Weapons.AXE) {
			this.weaponChanged = true;
			this.setWeapon(Weapons.AXE);
		}
	}
	else if(weapon > 189 && weapon <= 252){
		if(this.equipedWeapon != Weapons.BOW) {
			this.weaponChanged = true;
			this.setWeapon(Weapons.BOW);
		}
	}

	setTimeout(function() {
		Player.changeWeapon();
	}, 10000);
};



Player.update = function() {
	if(!this.health <= 0 && !this.endGame) {
		if(this.weaponChanged) {
			this.smoke.update();
			if(this.smoke.looped >= 1) {
				this.weaponChanged = false;
			}
		} else {
			this.smoke.reset();
		}

		if(new Date().getTime() > this.speedBoostTimer + this.speedBoostTime * 1000) {
			this.speedBoost = 0;
			this.speedBoostTimer = 0;
		}

		if(new Date().getTime() > this.invincibleTimer + this.invincibleTime * 1000) {
			this.invincibleTimer = 0;
		}
		this.updateAnimations();
		this.checkInputs();
		// Apply gravity
		for(var i = 0; i < Math.floor(this.gravity); i++) {
			if(this.jump) {

			}
			else if(!this.checkCollisonYDown()) {
				this.pos.y++; 
				this.gravity += 0.05;
			} else {
				this.gravity = 3;
				this.jumpCount = 0;
			} 
		}

		if(this.jump) {
			for(var i = 0; i < this.speed * 3; i++) {
				if(!this.checkCollisonYUp()) {
					Player.pos.y--;
				} else {
					// Player has collided with something, stop jumping
					this.jump = false;
					// Allow the player to jump again without waiting for timer
					this.jumpTimer -= 100;
				}
			}

			if(Math.abs(this.pos.y - this.jumpOrigPos.y) >= 90) {
				this.jump = false;
			}

		}
		
		// Standing on spikes kills the player
		if(this.isStandingOnTileType(10)) {
			if(this.invincibleTimer == 0) {
				this.health = 0;
			}
		}

	} else {
		this.smoke.update();
		if(this.smoke.looped >= 1) {
			if(this.endGame) {
				this.ranOutOfTime();
			} else {
				this.die();
			}

		}
	}
};

Player.isStandingOnTileType = function(type) {
	var nextBottomY = this.pos.y + this.height * RENDER_SCALE_MULTIPLIER + 1;
	var nextTileY = nextBottomY / 32 | 0;
	var tileX1 = this.pos.x / 32 | 0;
	var tileX2 = (this.pos.x + this.width * RENDER_SCALE_MULTIPLIER) / 32 | 0;
	
	return LEVEL_DATA[nextTileY][tileX1] == type ||
		LEVEL_DATA[nextTileY][tileX2] == type;
}

Player.updateAnimations = function() {
	if(this.moving) {
		this.currentLeftAnim.update();
		this.currentRightAnim.update();
	} else {
		this.currentLeftAnim.reset();
		this.currentRightAnim.reset();
	}
}

Player.attack = function() {
	if(new Date().getTime() > this.attackTimer + 200) {
		switch(this.equipedWeapon) {
			case Weapons.NONE: 
				break;
			case Weapons.SWORD:
				for(var i = 0; i < mobs.length; i++) {
					var box1 = {
						x1: this.pos.x + this.width * RENDER_SCALE_MULTIPLIER,
						x2: this.pos.x + (this.width * RENDER_SCALE_MULTIPLIER) * 2,
						y1: this.pos.y,
						y2: this.pos.y + this.height * RENDER_SCALE_MULTIPLIER
					}
					var box2 = {
						x1: this.pos.x - this.width * RENDER_SCALE_MULTIPLIER,
						x2: this.pos.x,
						y1: this.pos.y,
						y2: this.pos.y + this.height * RENDER_SCALE_MULTIPLIER
					}
					if((mobs[i].pos.x >= box1.x1 && mobs[i].pos.x <= box1.x2 &&
						mobs[i].pos.y >= box1.y1 && mobs[i].pos.y <= box1.y2 && !this.movingLeft) ||
						mobs[i].pos.x >= box2.x1 && mobs[i].pos.x <= box2.x2 &&
						(mobs[i].pos.y >= box2.y1 && mobs[i].pos.y <= box2.y2 && this.movingLeft)) {
						mobs[i].knockBack(this.movingLeft, 50);
						mobs[i].alterHealth(1);
						AudioManager.meleeHit.play();
						break;
					}
				}
				break;
			case Weapons.SPEAR:
				for(var i = 0; i < mobs.length; i++) {
					var box1 = {
						x1: this.pos.x + (this.width * RENDER_SCALE_MULTIPLIER) * 1,
						x2: this.pos.x + (this.width * RENDER_SCALE_MULTIPLIER) * 3,
						y1: this.pos.y,
						y2: this.pos.y + this.height * RENDER_SCALE_MULTIPLIER
					}
					var box2 = {
						x1: this.pos.x - (this.width * RENDER_SCALE_MULTIPLIER) * 2,
						x2: this.pos.x,
						y1: this.pos.y,
						y2: this.pos.y + this.height * RENDER_SCALE_MULTIPLIER
					}
					if((mobs[i].pos.x >= box1.x1 && mobs[i].pos.x <= box1.x2 &&
						mobs[i].pos.y >= box1.y1 && mobs[i].pos.y <= box1.y2 && !this.movingLeft) ||
						mobs[i].pos.x >= box2.x1 && mobs[i].pos.x <= box2.x2 &&
						(mobs[i].pos.y >= box2.y1 && mobs[i].pos.y <= box2.y2 && this.movingLeft)) {
						mobs[i].knockBack(this.movingLeft, 50);
						mobs[i].alterHealth(1);
						AudioManager.meleeHit.play();
						break;
					}
				}
				break;
			case Weapons.AXE:
				for(var i = 0; i < mobs.length; i++) {
					var box1 = {
						x1: this.pos.x + this.width * RENDER_SCALE_MULTIPLIER,
						x2: this.pos.x + (this.width * RENDER_SCALE_MULTIPLIER) * 2,
						y1: this.pos.y,
						y2: this.pos.y + this.height * RENDER_SCALE_MULTIPLIER
					}
					var box2 = {
						x1: this.pos.x - this.width * RENDER_SCALE_MULTIPLIER,
						x2: this.pos.x,
						y1: this.pos.y,
						y2: this.pos.y + this.height * RENDER_SCALE_MULTIPLIER
					}
					if((mobs[i].pos.x >= box1.x1 && mobs[i].pos.x <= box1.x2 &&
						mobs[i].pos.y >= box1.y1 && mobs[i].pos.y <= box1.y2 && !this.movingLeft) ||
						mobs[i].pos.x >= box2.x1 && mobs[i].pos.x <= box2.x2 &&
						(mobs[i].pos.y >= box2.y1 && mobs[i].pos.y <= box2.y2 && this.movingLeft)) {
						mobs[i].knockBack(this.movingLeft, 500);
						mobs[i].alterHealth(1);
						AudioManager.meleeHit.play();
						break;
					}
				}
				break;
			case Weapons.BOW:
				this.moving = true;
				// fire projectile
				projectiles.push(new Projectile(this.pos.x + this.width / 2, this.pos.y + 22, this.movingLeft));
				break;
			case Weapons.SHIELD:
				break;
		}
		this.attackTimer = new Date().getTime();
	}

}

Player.getPlayerRenderPos = function() {
	var canvasCenter = Utils.getCanvasCenter();
	var levelWidth = LEVEL_DATA[0].length * 32;
    var levelHeight = LEVEL_DATA.length * 32;
	
	var playerDrawX = canvasCenter.x - this.width / 2;
	var playerDrawY = canvasCenter.y - this.height / 2;
	
	if (this.pos.x < 960/2) {
		// Player is at left edge of the map
		playerDrawX -= 960/2 - Player.pos.x;
	} else if (this.pos.x > (levelWidth - 960/2 - 32)) {
        // Player is at right edge of map
		playerDrawX += this.pos.x - (levelWidth - 960/2) + 32;
	}
	
	if (this.pos.y < (550/2)) {
		// Player is at top of the map
		playerDrawY -= 550/2 - Player.pos.y;
	} else if (this.pos.y > (levelWidth - 550/2) - 32) {
        // Player is at bottom of map
		playerDrawX += this.pos.y - (levelHeight - 550/2) + 32;
	}
	return {x: playerDrawX, y: playerDrawY};
}

Player.render = function(ctx) {
	var drawPos = this.getPlayerRenderPos();
	if(!this.health <= 0 && !this.endGame) {
		var canvasCenter = Utils.getCanvasCenter();
		
		this.renderWeapon(ctx, canvasCenter, function() {
			var playerImage = ImageLoader.images["player-spritesheet"];
			if(Player.moving) {
				Player.renderAnimationSprites(ctx, canvasCenter, drawPos);
			} else {
				if(Player.movingLeft) {
					ctx.drawImage(playerImage, 6 * Player.width, Player.height, Player.width, Player.height,
						drawPos.x, drawPos.y,
						Player.width * RENDER_SCALE_MULTIPLIER, Player.height * RENDER_SCALE_MULTIPLIER);
				} else {
					ctx.drawImage(playerImage, 6 * Player.width, 0, Player.width, Player.height,
						drawPos.x, drawPos.y,
						Player.width * RENDER_SCALE_MULTIPLIER, Player.height * RENDER_SCALE_MULTIPLIER);
				}

			}
		});

		this.renderHealth(ctx);
	} else if(this.smoke.looped < 1) {
		var params = {
			ctx: ctx,
			x: drawPos.x,
			y: drawPos.y,
			renderWidth: 23 * RENDER_SCALE_MULTIPLIER,
			renderHeight: 18 * RENDER_SCALE_MULTIPLIER
		}
		this.smoke.render(params);
	}
};

Player.renderAnimationSprites = function(ctx, canvasCenter, drawPos) {
	var params = {
		ctx: ctx,
		x: drawPos.x,
		y: drawPos.y,
		renderWidth: this.width * RENDER_SCALE_MULTIPLIER,
		renderHeight: this.height * RENDER_SCALE_MULTIPLIER
	}
	if(this.movingLeft) {
		this.currentLeftAnim.render(params);
	} else {
		this.currentRightAnim.render(params);
	}
}

Player.setWeapon = function(weapon) {
	this.equipedWeapon = weapon;
	switch(this.equipedWeapon) {
		case Weapons.NONE: 
			this.currentLeftAnim = this.animations[0];
			this.currentRightAnim = this.animations[1];
			break;
		case Weapons.SWORD:
		case Weapons.SPEAR:
		case Weapons.AXE:

			this.currentLeftAnim = this.animations[2];
			this.currentRightAnim = this.animations[3];
			break;
		case Weapons.BOW:
			this.currentLeftAnim = this.animations[4];
			this.currentRightAnim = this.animations[5];
			break;
		case Weapons.SHIELD:
			this.currentLeftAnim = this.animations[6];
			this.currentRightAnim = this.animations[7];
			break;
	}
}

Player.renderWeapon = function(ctx, canvasCenter, func) {
	if(this.movingLeft) {
		switch(this.equipedWeapon) {
			case Weapons.SWORD:
				this.renderSword(ctx, canvasCenter, 27, 0, 27, 27, true);
				func();
				break;
			case Weapons.SPEAR:
				func();
				this.renderSpear(ctx, canvasCenter, 40, 56, 40, 40, true);
				break;
			case Weapons.AXE:
				this.renderSword(ctx, canvasCenter, 29, 27, 29, 29, true);
				func();
				break;
			case Weapons.BOW:
				func();
				this.renderBow(ctx, canvasCenter, 19, 96, 19, 24, true, func);
				break;
			case Weapons.SHIELD:
				break;
		}
		if(this.weaponChanged) {
			this.renderSmoke(-15, 20);
		}
	} else {
		switch(this.equipedWeapon) {
			case Weapons.SWORD:
				func();
				this.renderSword(ctx, canvasCenter, 0, 0, 27, 27, false);
				break;
			case Weapons.SPEAR:
				func();
				this.renderSpear(ctx, canvasCenter, 0, 56, 40, 40, false);
				break;
			case Weapons.AXE:
				func();
				this.renderSword(ctx, canvasCenter, 0, 27, 29, 29, false);
				break;
			case Weapons.BOW:
				this.renderBow(ctx, canvasCenter, 0, 96, 19, 24, false, func);
				break;
			case Weapons.SHIELD:
				break;
		}
		if(this.weaponChanged) {
			this.renderSmoke(10, 20);
		}
	}
}

Player.renderSmoke = function(xOffset, yOffset) {
	var drawPos = this.getPlayerRenderPos();
	var params = {
		ctx: ctx,
		x: drawPos.x + xOffset,
		y: drawPos.y + yOffset,
		renderWidth: 23 * RENDER_SCALE_MULTIPLIER,
		renderHeight: 18 * RENDER_SCALE_MULTIPLIER
	}
	this.smoke.render(params);
}

Player.renderSpear = function(ctx, canvasCenter, spriteX, spriteY, width, height, isLeft) {
	var image = ImageLoader.images["weapons"];
	var playerDrawPos = this.getPlayerRenderPos();
	
	if(isLeft) {
		var Xadditive = -12;
		var Yadditive = 10;
		if(this.currentRightAnim.frame == 0 || this.currentRightAnim.frame == 3) {
			Yadditive = 12;
			Xadditive = -14;
		}
		if(Player.isLeftClick) {
			Xadditive -= 5;
			Yadditive -= 2;
		}
		var x = playerDrawPos.x - (this.width * 2 - 15) + Xadditive;
		var y = (playerDrawPos.y + 3) + Yadditive;

		ctx.save(); // save current state
		ctx.translate(x + 20, y + 20);
		ctx.rotate(330*Math.PI/180); // rotate
		ctx.translate(-(x + 20), -(y + 20));
		ctx.drawImage(image, spriteX, spriteY, width, height,
			x, y,
			40 * RENDER_SCALE_MULTIPLIER, 40  * RENDER_SCALE_MULTIPLIER);
		ctx.restore();
	} else {
		var Xadditive = 40;
		var Yadditive = -10;
		if(this.currentRightAnim.frame == 0 || this.currentRightAnim.frame == 3) {
			Yadditive = -8;
			Xadditive = 42;
		}
		if(Player.isLeftClick) {
			Xadditive += 5;
			Yadditive -= 2;
		}
		var x = playerDrawPos.x - (this.width * 2 - 15) + Xadditive;
		var y =(playerDrawPos.y + 3) + Yadditive;

		ctx.save(); // save current state
		ctx.translate(x + 20, y + 20);
		ctx.rotate(35*Math.PI/180); // rotate
		ctx.translate(-(x + 20), -(y + 20));
		ctx.drawImage(image, spriteX, spriteY, width, height,
			x, y,
			40 * RENDER_SCALE_MULTIPLIER, 40  * RENDER_SCALE_MULTIPLIER);
		ctx.restore();
	}
}

Player.renderBow = function(ctx, canvasCenter, spriteX, spriteY, width, height, isLeft, func) {
	var image = ImageLoader.images["weapons"];
	var playerDrawPos = this.getPlayerRenderPos();
	
	if(isLeft) {
		if(this.moving) {
			func();
			var Xadditive = 15;
			var Yadditive = 7;
			var x = playerDrawPos.x - (this.width * 2 - 15) + Xadditive;
			var y =(playerDrawPos.y + 3) + Yadditive;
			ctx.drawImage(image, spriteX, spriteY, width, height,
				x, y,
				20 * RENDER_SCALE_MULTIPLIER, 20  * RENDER_SCALE_MULTIPLIER) + Yadditive;
		} else {
			var x = playerDrawPos.x - (this.width * 2 - 30);
			var y =(playerDrawPos.y + 32);
			ctx.drawImage(image, 38, 109, 27, 11,
				x, y,
				27 * RENDER_SCALE_MULTIPLIER, 11 * RENDER_SCALE_MULTIPLIER);
			func();
		}
	} else {
		if(this.moving) {
			func();
			var Xadditive = -15;
			var Yadditive = 7;
			var x = playerDrawPos.x + (this.width * 2 - 15) + Xadditive;
			var y = (playerDrawPos.y + 3) + Yadditive;
			ctx.drawImage(image, spriteX, spriteY, width, height,
				x, y,
				20 * RENDER_SCALE_MULTIPLIER, 20  * RENDER_SCALE_MULTIPLIER);
		} else {
			var x = playerDrawPos.x + (this.width * 2 - 38);
			var y = (playerDrawPos.y + 32);
			ctx.drawImage(image, 38, 109, 27, 11,
				x, y,
				27 * RENDER_SCALE_MULTIPLIER, 11 * RENDER_SCALE_MULTIPLIER);
			func();
		}
	}
	if(Player.isLeftClick) {
 		ctx.restore(); // restore original states (no rotation etc)
 	}
}

Player.renderSword = function(ctx, canvasCenter, spriteX, spriteY, width, height, isLeft) {
	var image = ImageLoader.images["weapons"];
	var playerDrawPos = this.getPlayerRenderPos();
	
	if(isLeft) {
		var Xadditive = 3;
		var Yadditive = 0;
		if(!this.moving) {
			Yadditive = 4;
			Xadditive = 4;
		}
		else if(this.currentLeftAnim.frame == 0 || this.currentLeftAnim.frame == 3) {
			Yadditive = 2;
			Xadditive = 1;
		}
		var x = playerDrawPos.x - (this.width * 2 - 15) + Xadditive;
		var y =(playerDrawPos.y + 3) + Yadditive;
		if(Player.isLeftClick) {
			ctx.save(); // save current state
			ctx.translate(x + 20, y + 20);
			ctx.rotate(300*Math.PI/180); // rotate
			ctx.translate(-(x + 20), -(y + 20));
			x -= 20;
			y += 6;
		}
		ctx.drawImage(image, spriteX, spriteY, width, height,
			x, y,
			20 * RENDER_SCALE_MULTIPLIER, 20  * RENDER_SCALE_MULTIPLIER);
	} else {	
		var Xadditive = 0;
		var Yadditive = 0;
		if(!this.moving) {
			Yadditive = 4;
			Xadditive = -2;
		}
		else if(this.currentRightAnim.frame == 0 || this.currentRightAnim.frame == 3) {
			Yadditive = 2;
			Xadditive = 2;
		}

		var x = playerDrawPos.x + (this.width * 2 - 15) + Xadditive;
		var y = (playerDrawPos.y + 3) + Yadditive;
		if(Player.isLeftClick) {
			ctx.save(); // save current state
			ctx.translate(x, y + 20);
			ctx.rotate(60*Math.PI/180); // rotate
			ctx.translate(-x, -(y + 20));
			x += 10;
			y -= 13;
		}
		ctx.drawImage(image, spriteX, spriteY, width, height,
			x, y,
			20 * RENDER_SCALE_MULTIPLIER, 20  * RENDER_SCALE_MULTIPLIER);
	}
	if(Player.isLeftClick) {
 		ctx.restore(); // restore original states (no rotation etc)
 	}
}

Player.checkInputs = function(e) {
	if(new Date().getTime() > this.attackTimer + 500) {
		this.moving = false;
	}
	if(keys[87]) {
		this.moving = true;
		for(var i = 0; i < this.speed + this.speedBoost; i++) {
			if(!this.checkCollisonYUp()) {
				Player.pos.y--;
			}
		}
	}

	if(keys[83]) {
		this.moving = true;
		for(var i = 0; i < this.speed + this.speedBoost; i++) {
			if(!this.checkCollisonYDown()) {
				Player.pos.y++;
			}
		}
	}

	if(keys[65]) {
		this.moving = true;
		this.movingLeft = true;
		for(var i = 0; i < this.speed + this.speedBoost; i++) {
			if(!this.checkCollisonXLeft()) {
				Player.pos.x--;
			}
		}
	}

	if(keys[68]) {
		this.moving = true;
		this.movingLeft = false;
		for(var i = 0; i < this.speed + this.speedBoost; i++) {
			if(!this.checkCollisonXRight()) {
				Player.pos.x++;
			}	
		}
	}
}

Player.onKeyUp = function(e) {
	switch(e.keyCode) {
		case 32: // Space
			this.jumpPressed = false;
			break;
	}
}

Player.onKeyDown = function(e) {
	switch(e.keyCode) {
		case 32: // Space
			if(!this.jumpPressed) {		
				if(new Date().getTime() > this.jumpTimer + 100) {
					if(this.jumpCount < 2) {
						this.jumpCount++;
						this.jump = true;
						this.jumpPressed = true;
						this.jumpOrigPos = {
							x: this.pos.x,
							y: this.pos.y
						}
						this.jumpTimer = new Date().getTime();
						AudioManager.jump.play();

					}
				}
			}
			break;
	}
}

Player.checkCollisonYDown = function() {
	var nextBottomY = this.pos.y + this.height * RENDER_SCALE_MULTIPLIER + 1;
	var nextTileY = nextBottomY / 32 | 0;
	var tileX1 = this.pos.x / 32 | 0;
	var tileX2 = (this.pos.x + this.width * RENDER_SCALE_MULTIPLIER) / 32 | 0;
	
	return Utils.isTileTypeSolid(LEVEL_DATA[nextTileY][tileX1]) ||
		Utils.isTileTypeSolid(LEVEL_DATA[nextTileY][tileX2]);
}

Player.checkCollisonYUp = function() {
	var nextTopY = this.pos.y - 1;
	var nextTileY = nextTopY / 32 | 0;
	var tileX1 = this.pos.x / 32 | 0;
	var tileX2 = (this.pos.x + this.width * RENDER_SCALE_MULTIPLIER) / 32 | 0;
	
	return Utils.isTileTypeSolid(LEVEL_DATA[nextTileY][tileX1]) ||
		Utils.isTileTypeSolid(LEVEL_DATA[nextTileY][tileX2]);
}

Player.checkCollisonXLeft = function() {
	var nextLeftX = this.pos.x - 1;
	var nextTileX = nextLeftX / 32 | 0;
	var tileY1 = this.pos.y / 32 | 0;
	var tileY2 = (this.pos.y + this.height * RENDER_SCALE_MULTIPLIER) / 32 | 0;
	
	return Utils.isTileTypeSolid(LEVEL_DATA[tileY1][nextTileX]) ||
		Utils.isTileTypeSolid(LEVEL_DATA[tileY2][nextTileX]);
}

Player.checkCollisonXRight = function() {
	var nextRightX = this.pos.x + this.width * RENDER_SCALE_MULTIPLIER + 1;
	var nextTileX = nextRightX / 32 | 0;
	var tileY1 = this.pos.y / 32 | 0;
	var tileY2 = (this.pos.y + this.height * RENDER_SCALE_MULTIPLIER) / 32 | 0;
	
	return Utils.isTileTypeSolid(LEVEL_DATA[tileY1][nextTileX]) ||
		Utils.isTileTypeSolid(LEVEL_DATA[tileY2][nextTileX]);
}

Player.renderHealth = function(ctx) {
	if(Player.invincibleTimer > 0) {
		ctx.fillStyle = '#fff200';
	} 
	else if(Player.health <= 5 && Player.health >= 3){
		ctx.fillStyle = '#f39c12';
	}
	else if(Player.health <= 3){
		ctx.fillStyle = "#c0392b";
	}
	ctx.font = "20px Righteous";
	var playerDrawPos = this.getPlayerRenderPos();
	ctx.textAlign="center"; 

	ctx.fillText(this.health + "/10", playerDrawPos.x + this.width, playerDrawPos.y - this.height/2);
}

Player.alterHealth = function(amount) {
	// Means player isnt invincible so can hurt player
	if(Player.invincibleTimer == 0) {
		if(this.health - amount >= 0) {
			this.health -= amount;
		} else {
			this.health = 0;
		}
	}
}

Player.die = function() {
	ctx.canvas.removeEventListener('mousedown', Player.onMouseDown, false);
	ctx.canvas.removeEventListener('mouseup', Player.onMouseUp, false);
	ctx.canvas.removeEventListener('click', Player.onMouseClick, false);

	gameState = GameStates.DEATH;
}

Player.ranOutOfTime = function() {
	ctx.canvas.removeEventListener('mousedown', Player.onMouseDown, false);
	ctx.canvas.removeEventListener('mouseup', Player.onMouseUp, false);
	ctx.canvas.removeEventListener('click', Player.onMouseClick, false);

	gameState = GameStates.OUT_OF_TIME;
}