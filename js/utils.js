var Utils = { 
	clientId: "05cd872a783dea265f9542c9c15ae71f"
};

/**
 * 	Returns the primary canvas element
 */
Utils.getCanvas = function() {
	return document.getElementById('canvas');
};

/**
 * 	Returns the primary canvas element width and height
 */
Utils.getCanvasSize = function() {
	return {
		width: this.getCanvas().width,
		height: this.getCanvas().height
	};
};

/**
 * 	Returns the primary canvas element center x and y
 */
Utils.getCanvasCenter = function() {
	return {
		x: this.getCanvas().width / 2,
		y: this.getCanvas().height / 2
	};
};

/**
 * Divides the height by an amount of divisions and returns the resulting positions
 */
Utils.divideHeight = function(numberOfDivisions) {
	var size = this.getCanvasSize();
	var division = (size.height) / numberOfDivisions;

	var array = [];
	for(var i = 0; i < numberOfDivisions; i++) {
		array.push((division * i) + (division / 2));
	}
	return array;
}

/**
 * Sends an xmlhttp get request
 */
Utils.sendGetRequest = function(url, callback) {
	var oReq = new XMLHttpRequest();
	//oReq.onload = reqListener;
	oReq.open("get", url, true);
	oReq.send();

	oReq.onreadystatechange = function() {
		if (oReq.readyState != 4)  { return; }

		var serverResponse = JSON.parse(oReq.responseText);
		callback(serverResponse);
	};
}

/**
 * Gets a soundcloud audio track and preloads it into an audio element
 */
Utils.getSoundcloudTrack = function(url, audioElement, callback) {
	var theURL = 'https://api.soundcloud.com/resolve?url=' + url + '&client_id=' + Utils.clientId;
	Utils.sendGetRequest(theURL, function(response) {
		musicTrack = response;
		audioElement.crossOrigin = "anonymous";
		audioElement.src = response.stream_url + "?client_id=" + Utils.clientId;
		//audioElement.autoplay = true;
		audioElement.preload = true;
		callback(response.stream_url != null ? true : false);
	});
};

/**
 * Gets a random soundcloud audio track from a user and preloads it into an audio element
 */
Utils.getRandomTrackFromUser = function(user, audioElement, callback) {
	var theURL = 'https://api.soundcloud.com/resolve?url=' + user + '&client_id=' + Utils.clientId;
	Utils.sendGetRequest(theURL, function(response) {
		if(response.uri != null) {
			Utils.sendGetRequest(response.uri + '/tracks' + '?client_id=' + Utils.clientId, function(response) {
				var tracks = response;
				var trackUrl;
				var trackList = [];
				for(var i = 0; i < tracks.length; i++) {
					if(tracks[i].duration / 1000 >= 120) {
						trackList.push(tracks[i]);
					}
				}
				if(trackList.length > 0) {
					trackUrl = trackList[Math.floor(Math.random() * trackList.length)].stream_url;
					audioElement.crossOrigin = "anonymous";
					audioElement.src = trackUrl + "?client_id=" + Utils.clientId;
					audioElement.preload = true;
				}
				musicTrack = trackList[Math.floor(Math.random() * trackList.length)];
				callback(trackUrl != null ? true : false);
			});
		} else {
			callback(false);
		}
	});
};

Utils.getMobDrawPosition = function(x, y) {
	var canvasCenter = this.getCanvasCenter();
	var levelWidth = LEVEL_DATA[0].length * 32;
    var levelHeight = LEVEL_DATA.length * 32;
	
	//Current tile that the player is located on
    var playerTileX = Player.pos.x / 32 | 0;
    var playerTileY = Player.pos.y / 32 | 0;
    
    //Offset from top-left of current tile of player
    var playerTileOffsetX = Player.pos.x % 32;
    var playerTileOffsetY = Player.pos.y % 32;
	
	if (Player.pos.x < 960/2) {
        // Player is at left edge of map
        playerTileX = 15;
        playerTileOffsetX = 0;
    } else if (Player.pos.x > (levelWidth - 960/2 - 32)) {
        // Player is at right edge of map
        playerTileX = (levelWidth / 32) - (960/2/32) - 1;
        playerTileOffsetX = 0;
    }
	
	if (Player.pos.y < 550/2) {
        // Player is at top of map
        playerTileY = 8;
        playerTileOffsetY = 19;
    } else if (Player.pos.y > (levelWidth - 550/2 - 32)) {
        // Player is at bottom of map
        playerTileY = (levelWidth / 32) - 8 - 1;
        playerTileOffsetY = 19;
    }
	
	var mobDrawX = playerTileX * -32 - playerTileOffsetX + canvasCenter.x - 16 + x;
    var mobDrawY = playerTileY * -32 - playerTileOffsetY + canvasCenter.y - 16 + y;
	
	return {
		x: mobDrawX,
		y: mobDrawY
	}
}

Utils.isTileTypeSolid = function(type) {
	return type != 0;
}