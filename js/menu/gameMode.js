var Mode = {
	selectedItem: 0,
	items: [
		{
			text: 'Casual',
			action: function() {
				level = "casual";
				initGame();
				gameState = GameStates.PLAYING;
			}
		},
		{
			text: 'Hardcore',
			action: function() {
				level = "hardcore";
				initGame();
				gameState = GameStates.PLAYING;
			}
		},
	]
};

Mode.render = function() {
	var ModePositions = Utils.divideHeight(3);
	var center = Utils.getCanvasCenter();

	ctx.fillStyle = '#102421';
	ctx.textAlign="center"; 

	ctx.font = "700 72px Bangers";
	ctx.fillText('Game Mode', center.x,  ModePositions[0]);

	ctx.fillStyle ="#306337";
	ctx.font = "36px'Righteous'";
	for(var i = 0; i < this.items.length; i++) {
		var textToRender = "";
		if(this.selectedItem === i) {
			textToRender = '< ' + this.items[i].text + ' >';
		} else {
			textToRender = this.items[i].text;
		}
		ctx.fillText(textToRender, center.x,  ModePositions[i+1]);
	}
}

Mode.changeItem = function(val) {
	this.selectedItem += val;
	if (this.selectedItem < 0) {
		this.selectedItem = this.items.length - 1;
	} else if (this.selectedItem >= this.items.length) {
		this.selectedItem = 0;
	}
};

Mode.select = function() {
	this.items[this.selectedItem].action();
};

Mode.onKeyDown = function(e) {
	switch(e.keyCode) {
		case 38: // Up Arrow 
			this.changeItem(-1);
			break;
		case 40: // Down Arrow 
			this.changeItem(+1);
			break;
		case 13: // Enter Key
			this.select();
			break;
	}
};