var Win = {};

Win.render = function(ctx) {
	AudioManager.stop();
	var winPositions = Utils.divideHeight(4);
	var center = Utils.getCanvasCenter();

	ctx.textAlign="center"; 
	ctx.fillStyle = '#ffffff';
	ctx.font = "72px Bangers";
	ctx.fillText('Congratulations!', center.x,  winPositions[0]);
	ctx.fillText('You won', center.x,  winPositions[1]);

	ctx.fillStyle ="#ffffff";
	ctx.font = "36px'Righteous'";
	ctx.fillText('< Menu >', center.x,  winPositions[3]);
};

Win.onKeyDown = function(e) {
	switch(e.keyCode) {
		case 13: // Enter Key
			gameState = GameStates.MENU;
			break;
	}
};