var Death = {};

Death.render = function() {
	AudioManager.stop();
	var deathPositions = Utils.divideHeight(4);
	var center = Utils.getCanvasCenter();

	ctx.fillStyle = '#c0392b';
	ctx.textAlign="center"; 
	ctx.font = "72px Bangers";
	ctx.fillText('Ouch!', center.x,  deathPositions[0]);

	ctx.fillStyle = '#ffffff';
	ctx.font = "72px Bangers";
	ctx.fillText('You died', center.x,  deathPositions[1]);

	ctx.fillStyle ="#ffffff";
	ctx.font = "36px'Righteous'";
	ctx.fillText('< Menu >', center.x,  deathPositions[3]);
};

Death.onKeyDown = function(e) {
	switch(e.keyCode) {
		case 13: // Enter Key
			gameState = GameStates.MENU;
			break;
	}
};