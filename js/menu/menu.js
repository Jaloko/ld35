var Menu = { 
	selectedItem: 0,
	items: [
		{
			text: 'Start',
			action: function() {
				gameState = GameStates.MODE;
			}
		},
		{
			text: 'Instructions',
			action: function() {
				gameState = GameStates.INSTRUCTIONS;
			}
		},
		{
			text: 'Change Track',
			action: function() {
				window.location.reload(false);
			}
		}
	]
};

Menu.render = function(ctx) {
	var menuPositions = Utils.divideHeight(this.items.length + 1);

	var center = Utils.getCanvasCenter();

	ctx.fillStyle = '#102421';
	ctx.textAlign="center"; 

	ctx.font = "72px Bangers";
	ctx.fillText('WeaponShift', center.x,  menuPositions[0]);
	ctx.fillStyle ="#306337";
	ctx.font = "36px'Righteous'";

	for(var i = 0; i < this.items.length; i++) {
		var textToRender = "";
		if(this.selectedItem === i) {
			textToRender = '< ' + this.items[i].text + ' >';
		} else {
			textToRender = this.items[i].text;
		}
		ctx.fillText(textToRender, center.x,  menuPositions[i+1]);
	}

}

Menu.changeItem = function(val) {
	this.selectedItem += val;
	if (this.selectedItem < 0) {
		this.selectedItem = this.items.length - 1;
	} else if (this.selectedItem >= this.items.length) {
		this.selectedItem = 0;
	}
}

Menu.select = function() {
	this.items[this.selectedItem].action();
}

Menu.onKeyDown = function(e) {
	switch(e.keyCode) {
		case 38: // Up Arrow 
			this.changeItem(-1);
			break;
		case 40: // Down Arrow 
			this.changeItem(+1);
			break;
		case 13: // Enter Key
			this.select();
			break;
	}
}