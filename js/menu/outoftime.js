var OutOfTime = {};

OutOfTime.render = function() {
	AudioManager.stop();
	var timePositions = Utils.divideHeight(3);
	var center = Utils.getCanvasCenter();

	ctx.fillStyle = '#c0392b';
	ctx.textAlign="center"; 

	ctx.fillStyle = '#ffffff';
	ctx.font = "72px Bangers";
	ctx.fillText('Out of time', center.x,  timePositions[0]);

	ctx.fillStyle ="#ffffff";
	ctx.font = "36px'Righteous'";
	ctx.fillText('< Menu >', center.x,  timePositions[2]);
};

OutOfTime.onKeyDown = function(e) {
	switch(e.keyCode) {
		case 13: // Enter Key
			gameState = GameStates.MENU;
			break;
	}
};