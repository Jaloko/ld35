var Instructions = {};

Instructions.render = function() {
	var instructionPositions = Utils.divideHeight(5);
	var center = Utils.getCanvasCenter();

	ctx.fillStyle = '#102421';
	ctx.textAlign="center"; 
	ctx.font = "72px Bangers";
	ctx.fillText('Weaponshift - Instructions', center.x,  instructionPositions[0]);
	ctx.fillStyle ="#306337";
	ctx.font = "30px'Righteous'";
	ctx.fillText('You have the song duration to kill all skeletons.', center.x,  instructionPositions[1]);
	ctx.fillText('Your weapon will shapeshift based on the song frequencies.', center.x,  instructionPositions[2]);
	ctx.fillText('W A S D to move. Left click to attack. Space to jump/double jump.', center.x,  instructionPositions[3]);
	ctx.fillText('< Back >', center.x,  instructionPositions[4]);
}

Instructions.onKeyDown = function(e) {
	switch(e.keyCode) {
		case 13: // Enter Key
			gameState = GameStates.MENU;
			break;
	}
}