var Map = {}

Map.render = function(ctx) {
    var DRAW_DISTANCE = 17*2;
    var canvasCenter = Utils.getCanvasCenter();
    var levelWidth = LEVEL_DATA[0].length * 32;
    var levelHeight = LEVEL_DATA.length * 32;
    
    // Current tile that the player is located on
    var playerTileX = Player.pos.x / 32 | 0;
    var playerTileY = Player.pos.y / 32 | 0;
    
    // Offset from top-left of current tile of player
    var playerTileOffsetX = Player.pos.x % 32;
    var playerTileOffsetY = Player.pos.y % 32;
    
    var xMin = Math.max(0, playerTileX - DRAW_DISTANCE); // The left-most tile that we need to draw
    var xMax = Math.min(LEVEL_DATA[0].length, playerTileX + DRAW_DISTANCE); // The right-most tile that we need to draw
    var yMin = Math.max(0, playerTileY - DRAW_DISTANCE);
    var yMax = Math.min(LEVEL_DATA.length, playerTileY + DRAW_DISTANCE);
    
    if (Player.pos.x < 960/2) {
        // Player is at left edge of map
        playerTileX = 15;
        playerTileOffsetX = 0;
    } else if (Player.pos.x > (levelWidth - 960/2 - 32)) {
        // Player is at right edge of map
        playerTileX = (levelWidth / 32) - 15 - 1;
        playerTileOffsetX = 0;
    }
    
    if (Player.pos.y < 550/2) {
        // Player is at top of map
        playerTileY = 8;
        playerTileOffsetY = 19;
    } else if (Player.pos.y > (levelWidth - 550/2 - 32)) {
        // Player is at bottom of map
        playerTileY = (levelWidth / 32) - 8 - 1;
        playerTileOffsetY = 19;
    }
    
    // Draw background
    var bgImage = ImageLoader.images["bg"];
    var mapXOffset = (playerTileX * 32 + playerTileOffsetX) % (374 * 2);
    for (var i = 0-mapXOffset; i < 960 /*canvas width*/; i += 374 * 2) {
        ctx.drawImage(bgImage, 0, 0, 374, 275 /*bg image height*/, i, 0, 374 * 2, 275 * 2);        
    }
    
    // Draw tiles
    for (var y = yMin; y < yMax; y++) {
        for (var x = xMin; x < xMax; x++) {
            var tile = LEVEL_DATA[y][x];
            var tileX = (tile % 16) * 16;
            var tileY = (tile / 16 | 0) * 16;
            
            var tileDrawX = (playerTileX - x) * -32 - playerTileOffsetX + canvasCenter.x - 16;
            var tileDrawY = (playerTileY - y) * -32 - playerTileOffsetY + canvasCenter.y - 16;
            
            if (!(tileDrawX < -31 || tileDrawX > 960 || tileDrawY < -31 || tileDrawY > 550)) {
                var tileSheet = ImageLoader.images["tiles"];
            ctx.drawImage(tileSheet, tileX, tileY, 16, 16,
                tileDrawX, tileDrawY, 32, 32);
            }
        }
    }
}