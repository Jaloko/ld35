var Mob = function(x, y) {
	this.pos = {
		x: x,
		y: y
	},
	this.health = level == "hardcore" ? 7 : 5,
	this.maxHealth = level == "hardcore" ? 7 : 5,
	this.damage = level == "hardcore" ? 2 : 1,
	this.width = 16,
	this.height = 27,
	this.originalPos = this.pos,
	this.speed = 1,
	this.currentlyMoving = 0,
	this.dontWander = false,
	this.idleTimer = new Date().getTime(),
	this.attackTimer = new Date().getTime(),
	this.knockbackTimer = new Date().getTime(),
	this.knockbackInterval = 100,
	// True == left, False == right
	this.knockbackDir = false,
	this.knockback = false,
	this.moving = false,
	this.movingLeft = false,
	this.flagForDeletion = false,
	this.leftAnimation = new Animation({
		frames: ImageLoader.images["mob"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 1,
		maxFrames: 5
	}),
	this.rightAnimation = new Animation({
		frames: ImageLoader.images["mob"], 
		width: this.width,
		height: this.height,
		frameXStart: 0,
		frameYStart: 0,
		maxFrames: 5
	});
	this.deathAnim =  new Animation({
		frames: ImageLoader.images["smoke"], 
		width: 23,
		height: 18,
		frameXStart: -1,
		frameYStart: 0,
		maxFrames: 1
	});
}

Mob.prototype.getCenter = function() {
	return { x: this.pos.x + this.width / 2, y: this.pos.y + this.height / 2 };
}

Mob.prototype.update = function() {
	if(!this.health <= 0) {

		if(this.moving) {
			this.leftAnimation.update();
			this.rightAnimation.update();
		} else {
			this.leftAnimation.reset();
			this.rightAnimation.reset();
		}

		if(this.knockback) {
			if(this.knockbackDir) {		
				for(var i = 0; i < 3; i++) {
					if(this.checkBounds(-this.speed)) {		
						this.pos.x--;
					}
				}

			} else {
				for(var i = 0; i < 3; i++) {
					if(this.checkBounds(this.speed)) {
						this.pos.x++;
					}
				}
			}
		}
		else if(this.checkForPlayer()) {
			this.attackPlayer();
		} else {
			this.wander();
		}

		if(new Date().getTime() > this.knockbackTimer + this.knockbackInterval) {
			this.knockback = false;
		}
	} else {
		this.deathAnim.update();
		if(this.deathAnim.looped >= 1) {
			this.flagForDeletion = true;
		}
	}
}

Mob.prototype.checkForPlayer = function() {
	var xRange = this.width * 6;
	var yRange = this.height * 3;
	if(Player.pos.x + (Player.width / 2 * RENDER_SCALE_MULTIPLIER) >= (this.pos.x + this.width / 2 * RENDER_SCALE_MULTIPLIER) - xRange && 
		Player.pos.x + (Player.width / 2 * RENDER_SCALE_MULTIPLIER) <= (this.pos.x + this.width / 2 * RENDER_SCALE_MULTIPLIER) + xRange &&
		Player.pos.y + (Player.width / 2 * RENDER_SCALE_MULTIPLIER) >= (this.pos.y + this.height / 2 * RENDER_SCALE_MULTIPLIER) - yRange && 
		Player.pos.y + (Player.width / 2 * RENDER_SCALE_MULTIPLIER) <= (this.pos.y + this.height / 2 * RENDER_SCALE_MULTIPLIER) + yRange) {
		return true;
	} else {
		return false;
	}
}

Mob.prototype.attackPlayer = function() {
	var distance = Player.pos.x + (Player.width / 2 * RENDER_SCALE_MULTIPLIER) - (this.pos.x + (this.width / 2 * RENDER_SCALE_MULTIPLIER));
	// eNSURE
	if(distance > 0) {
		distance -=	Player.width * RENDER_SCALE_MULTIPLIER;
		this.movingLeft = false;
	} else {
		distance += Player.width * RENDER_SCALE_MULTIPLIER;
		this.movingLeft = true;
	}
	if(distance != 0) {
		this.moving = true;
		if(distance > 0) {
			if(this.checkBounds(this.speed)) {
				this.movingLeft = false;
				this.pos.x += this.speed;
			}
		} else {
			if(this.checkBounds(-this.speed)) {
				this.movingLeft = true;
				this.pos.x -= this.speed;
			}
		}
	}
	//console.log(distance + " " + this.width / 2 + " " + -this.width / 2);
	// Could do with a change in the future
	if(distance <= this.width / 2 && distance >= -this.width / 2){
		// Attack / damage player
		if(new Date().getTime() > this.attackTimer + 1000) {
			Player.alterHealth(this.damage);
			this.attackTimer = new Date().getTime();
		}
	}

}

Mob.prototype.wander = function() {
	this.moving = false;
	if(!this.dontWander) {
		this.currentlyMoving = Math.floor(Math.random() * 3);
		this.originalPos = {
			x: this.pos.x,
			y: this.pos.y
		}
		this.dontWander = true;
		this.idleTimer = new Date().getTime();
	}

	if(this.currentlyMoving === 0) {
		if(new Date().getTime() > this.idleTimer + 1000) {
			this.dontWander = false;
		}
	} else if(this.currentlyMoving === 1) {
		if(this.checkBounds(this.speed)) {
			this.moving = true;
			this.movingLeft = false;
			this.pos.x += this.speed;
			if(this.pos.x - this.originalPos.x >= 32) {
				this.dontWander = false;
			}
		} else {
			this.dontWander = false;
		}

	} else if(this.currentlyMoving === 2) {
		if(this.checkBounds(-this.speed)) {
			this.moving = true;
			this.movingLeft = true;
			this.pos.x -= this.speed;
			if(this.pos.x - this.originalPos.x <= -32) {
				this.dontWander = false;
			}
		} else {
			this.dontWander = false;
		}
	}

}

Mob.prototype.alterHealth = function(amount) {
	if(this.health - amount >= 0) {
		this.health -= amount;
	} else {
		this.health = 0;
	}
}

Mob.prototype.checkBounds = function(movementVal) {
	var additive = movementVal > 0 ? (this.width * RENDER_SCALE_MULTIPLIER / 2) - 1 : -(this.width * RENDER_SCALE_MULTIPLIER / 2);
	var pos = this.getCenter();
	var tileY = Math.floor((pos.y + (this.height * RENDER_SCALE_MULTIPLIER)) / 32) + 1;
	var tileX = Math.floor(((pos.x + additive) + movementVal) / 32);
	if(LEVEL_DATA[tileY][tileX] == 0 || LEVEL_DATA[tileY - 2][tileX] != 0 || LEVEL_DATA[tileY - 2][tileX] != 0) {
		return false;
	} else {
		return true;
	}
}

// Works
Mob.prototype.knockBack = function(dir, interval) {
	this.knockbackTimer = new Date().getTime();
	this.knockbackInterval = interval;
	this.knockbackDir = dir;
	this.knockback = true;
}
// Does not work ?????
/*Mob.prototype.knockback = function(dir) {
	this.knockbackTimer = new Date().getTime();
	this.knockbackDir = dir;
}*/

Mob.prototype.render = function(ctx) {
	var pos = Utils.getMobDrawPosition(this.pos.x, this.pos.y);
	if(!this.health <= 0) {
		var playerImage = ImageLoader.images["player"];
		//ctx.drawImage(playerImage, pos.x, pos.y, this.width, this.height);

		var mob = ImageLoader.images["mob"];
		if(this.moving) {
			this.renderAnimationSprites(ctx, pos);
		} else {
			if(this.movingLeft) {
				ctx.drawImage(mob, 6 * this.width, this.height, this.width, this.height,
				pos.x, pos.y, this.width * RENDER_SCALE_MULTIPLIER, this.height * RENDER_SCALE_MULTIPLIER);
			} else {
				ctx.drawImage(mob, 6 * this.width, 0, this.width, this.height,
				pos.x, pos.y, this.width * RENDER_SCALE_MULTIPLIER, this.height * RENDER_SCALE_MULTIPLIER);
			}

		}

		this.renderHealth(ctx, pos);
	} else if(!this.flagForDeletion) {
		var params = {
			ctx: ctx,
			x: pos.x,
			y: pos.y,
			renderWidth: 23 * RENDER_SCALE_MULTIPLIER,
			renderHeight: 18 * RENDER_SCALE_MULTIPLIER
		}
		this.deathAnim.render(params);
	}

}

Mob.prototype.renderAnimationSprites = function(ctx, pos) {
	var params = {
		ctx: ctx,
		x: pos.x,
		y: pos.y,
		renderWidth: this.width * RENDER_SCALE_MULTIPLIER,
		renderHeight: this.height * RENDER_SCALE_MULTIPLIER
	}
	if(this.movingLeft) {
		this.leftAnimation.render(params);
	} else {
		this.rightAnimation.render(params);
	}
}

Mob.prototype.renderHealth = function(ctx, pos) {
	ctx.fillStyle = '#ffffff';
	ctx.font = "20px Righteous";
	ctx.textAlign="center"; 
	ctx.fillText(this.health + "/" + this.maxHealth, pos.x + this.width * RENDER_SCALE_MULTIPLIER / 2, pos.y - this.height * RENDER_SCALE_MULTIPLIER / 2 + 10);
}

