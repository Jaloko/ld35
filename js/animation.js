var Animation = function(params) {
	this.frames = params.frames,
	this.frame = params.frameXStart,
	this.width = params.width,
	this.height = params.height,
	this.frameXStart = params.frameXStart,
	this.frameYStart = params.frameYStart
	this.maxFrames = params.maxFrames,
	this.speed = params.speed || 200,
	this.timer = new Date().getTime(),
	this.looped = 0;
}

Animation.prototype.update = function() {
	if(new Date().getTime() > this.timer + this.speed) {
		this.frame++;
		if(this.frame > this.maxFrames) {
			this.frame = this.frameXStart;
			this.looped++;
		}
		this.timer = new Date().getTime();
	}
}

Animation.prototype.render = function(params) {
	params.ctx.drawImage(
		this.frames, 
		this.width * this.frame, 
		this.frameYStart * this.height, 
		this.width, 
		this.height, 
		params.x, 
		params.y, 
		params.renderWidth, 
		params.renderHeight
	);
}

Animation.prototype.reset = function() {
	this.looped = 0;
	this.frame = this.frameXStart;
}