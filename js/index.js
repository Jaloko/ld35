var ctx;
var keys = [65565];
var GameStates = {
	MENU: 0,
	PLAYING: 1,
	INSTRUCTIONS: 2,
	DEATH: 3,
	MODE: 4,
	OUT_OF_TIME: 5,
	WIN: 6
};
var gameState = GameStates.MENU;

var level = "hardcore";

var mobs = []; 
var projectiles = [];
var potions = [];

var musicTrack = "";

function init() {
	var canvas = document.getElementById('canvas');
	ctx = canvas.getContext('2d');
	// Preload font
	ctx.font = "700 72px Bangers";
	ctx.fillText('Test', 0, 0);
	ctx.font = "36px'Righteous'";
	ctx.fillText('Test', 0, 0);

	ctx.mozImageSmoothingEnabled = false;
	ctx.webkitImageSmoothingEnabled = false;
	ctx.msImageSmoothingEnabled = false;
	ctx.imageSmoothingEnabled = false;
	
	ImageLoader.loadImages(function() {

	});	
}

function initGame() {
	// Init game objects
	Player.init();
	mobs = [];
	potions = [];
	//potions.push(new Potion(Potions.INVINCIBLE, 5*32, 22*32 + 7));
	//potions.push(new Potion(Potions.SPD, 5*32, 22*32 + 7));
		
	// Default Mobs
	mobs.push(new Mob(26*32, 15*32 - 22));
	mobs.push(new Mob(54*32, 29*32 - 22));
	mobs.push(new Mob(78*32, 21*32 - 22));
	mobs.push(new Mob(96*32, 29*32 - 22));
	mobs.push(new Mob(99*32, 18*32 - 22));
	mobs.push(new Mob(56*32, 15*32 - 22));
	mobs.push(new Mob(124*32, 23*32 - 22));
	mobs.push(new Mob(141*32, 37*32 - 22));
	mobs.push(new Mob(183*32, 33*32 - 22));
	mobs.push(new Mob(139*32, 23*32 - 22));
	mobs.push(new Mob(195*32, 31*32 - 22));

	potions.push(new Potion(Potions.HP, 23*32, 15*32 + 7));
	potions.push(new Potion(Potions.MAX_HP, 46*32, 29*32 + 7));
	potions.push(new Potion(Potions.SPD, 76*32, 27*32 + 7));
	potions.push(new Potion(Potions.INVINCIBLE, 109*32, 20*32 + 7));
	potions.push(new Potion(Potions.HP, 191*32, 31*32 + 7));
	// Hardcore maybe
	potions.push(new Potion(Potions.SPD, 197*32, 31*32 + 7));

	// Extra mobs on hardcore
	if(level === "hardcore"){
		mobs.push(new Mob(18*32, 23*32 - 22));
		mobs.push(new Mob(37*32, 19*32 - 22));
		mobs.push(new Mob(79*32, 27*32 - 22));
		mobs.push(new Mob(100*32, 37*32 - 22));
		mobs.push(new Mob(109*32, 27*32 - 22));
		mobs.push(new Mob(113*32, 43*32 - 22));
		mobs.push(new Mob(127*32, 41*32 - 22));
		mobs.push(new Mob(135*32, 23*32 - 22));
		mobs.push(new Mob(153*32, 26*32 - 22));
		mobs.push(new Mob(167*32, 31*32 - 22));
	}
	projectiles = [];


	document.getElementById('audio').play();
}

function checkTrackUrl() {
	//var url = "https://soundcloud.com/ignition-remixes/alan-walker-faded-radiology-remix";
	var url = document.getElementById('url').value;
	var audioElement = document.getElementById("audio");
	Utils.getSoundcloudTrack(url, audioElement, function(success) {
		if(success) {
			setKeyBindings();
			// Hide input html
			document.getElementById('track-container').className= "invisible";

			document.getElementById('canvas').className = "";

			document.getElementById('volume-container').className = "";

			document.getElementById('music-track-box').className = "";
			document.getElementById('music-track').innerHTML = "Song : " + musicTrack.title + " - ";
			document.getElementById('music-track').innerHTML += "<a href='" + musicTrack.permalink_url + "' target='_blank'>Click to hear it on Soundcloud</a";

			AudioManager.init();
			// Start game loop
			update();
		} else {
			// Show error in html
			document.getElementById('error').className = "red";
		}
	});

}

function selectRandomTrack() {
	var audioElement = document.getElementById("audio");
	Utils.getRandomTrackFromUser('https://soundcloud.com/kenek0317', audioElement, function(success) {
		if(success) {
			setKeyBindings();

			// Hide input html
			document.getElementById('track-container').className= "invisible";

			document.getElementById('canvas').className = "";

			document.getElementById('volume-container').className = "";
			document.getElementById('music-track-box').className = "";
			document.getElementById('music-track').innerHTML = "Song : " + musicTrack.title + " - ";
			document.getElementById('music-track').innerHTML += "<a href='" + musicTrack.permalink_url + "' target='_blank'>Hear it on Soundcloud</a";


			AudioManager.init();
			// Start game loop
			update();
		} else {
			// Show error in html
			document.getElementById('error').className = "red";
		}
	});
}

function update() {

	switch(gameState) {
		case GameStates.PLAYING:
			var currentTime = parseInt(document.getElementById('audio').currentTime);
			var duration = parseInt(document.getElementById('audio').duration);

			var timeLeft = duration - currentTime;
			if(!isNaN(timeLeft)) {
				//Game Win
				if(mobs.length <= 0) {

				}
				//Game Loss
				else if(timeLeft <= 0) {
					Player.endGame = true;
				}
				Player.update();
				AudioManager.update();
				// Delete flagged potions
				for(var i = 0; i < potions.length; i++) {
					if(potions[i].flagForDeletion) {
						potions.splice(i, 1);
					}
				}
				for(var i = 0; i < potions.length; i++) {
					potions[i].update();
				}
				// Delete flagged mobs
				for(var i = 0; i < mobs.length; i++) {
					if(mobs[i].flagForDeletion) {
						mobs.splice(i, 1);
					}
				}
				for(var i = 0; i < mobs.length; i++) {
					mobs[i].update();
				}

				// Delete flagged projectiles
				for(var i = 0; i < projectiles.length; i++) {
					if(projectiles[i].flagForDeletion) {
						projectiles.splice(i, 1);
					}
				}
				for(var i = 0; i < projectiles.length; i++) {
					projectiles[i].update();
				}

				if(mobs.length <= 0) {
					gameState = GameStates.WIN;
				}
			}

			break;
	}

	render();
	requestAnimationFrame(update);
}

function render() {
	switch(gameState) {
		case GameStates.MENU:
		case GameStates.INSTRUCTIONS:
		case GameStates.DEATH:
		case GameStates.MODE:
			ctx.fillStyle = '#6DA77C';
			break;
		default:
			ctx.fillStyle = '#6DA77C';
			break;
	}
	ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

	switch(gameState) {
		case GameStates.MENU:
			Menu.render(ctx);
			break;
		case GameStates.PLAYING:
			Map.render(ctx);
			for(var i = 0; i < potions.length; i++) {
				potions[i].render(ctx);
			}
			for(var i = 0; i < mobs.length; i++) {
				mobs[i].render(ctx);
			}
			for(var i = 0; i < projectiles.length; i++) {
				projectiles[i].render(ctx);
			}
			Player.render(ctx);

			var currentTime = parseInt(document.getElementById('audio').currentTime);
			var duration = parseInt(document.getElementById('audio').duration);
			var timeLeft = duration - currentTime;

			var seconds = timeLeft % 60 | 00;
			var mins = Math.floor( timeLeft / 60 ) % 60 | 00;

			seconds = seconds < 10 ? "0"+seconds : seconds;
			mins = mins < 10 ? "0"+mins : mins;

			ctx.font="15px Righteous";
			ctx.textAlign="left"; 
			ctx.fillText("Time Left: " + mins + ":" + seconds, 10,20);
			ctx.fillText("Skeletons left: " + mobs.length,10,40);
			if(Player.speedBoost > 0 && Player.invincibleTimer > 0) {
				var sSeconds = Player.speedBoostTime - Math.floor(((new Date().getTime() - Player.speedBoostTimer) / 1000));
				ctx.fillText("Speed Boost: " + sSeconds,10,60);
				sSeconds = Player.invincibleTime - Math.floor(((new Date().getTime() - Player.invincibleTimer) / 1000));
				ctx.fillText("Invincible: " + sSeconds,10,80);
			} 
			else if(Player.speedBoost > 0) {
				var sSeconds = Player.speedBoostTime - Math.floor(((new Date().getTime() - Player.speedBoostTimer) / 1000));
				ctx.fillText("Speed Boost: " + sSeconds,10,60);
			}
			else if(Player.invincibleTimer > 0) {
				var sSeconds = Player.invincibleTime - Math.floor(((new Date().getTime() - Player.invincibleTimer) / 1000));
				ctx.fillText("Invincible: " + sSeconds,10,60);
			}

			// Check if audio is ready to play
			var currentTime = parseInt(document.getElementById('audio').currentTime);
			var duration = parseInt(document.getElementById('audio').duration);
			var timeLeft = duration - currentTime;
			if(isNaN(timeLeft)) {
				var center = Utils.getCanvasCenter();
				ctx.font="32px Righteous";
				ctx.textAlign="center"; 
				ctx.fillText("Waiting on audio...", center.x, center.y);
			}
			break;
		case GameStates.INSTRUCTIONS:
			Instructions.render(ctx);
			break;

		case GameStates.DEATH:
			Map.render(ctx);
			for(var i = 0; i < potions.length; i++) {
				potions[i].render(ctx);
			}
			for(var i = 0; i < mobs.length; i++) {
				mobs[i].render(ctx);
			}
			for(var i = 0; i < projectiles.length; i++) {
				projectiles[i].render(ctx);
			}
			Player.render(ctx);
			ctx.font="15px Georgia";
			Death.render();
			break;
		case GameStates.OUT_OF_TIME:
			Map.render(ctx);
			for(var i = 0; i < potions.length; i++) {
				potions[i].render(ctx);
			}
			for(var i = 0; i < mobs.length; i++) {
				mobs[i].render(ctx);
			}
			for(var i = 0; i < projectiles.length; i++) {
				projectiles[i].render(ctx);
			}
			Player.render(ctx);
			ctx.font="15px Georgia";
			OutOfTime.render();
			break
		case GameStates.MODE:
			Mode.render(ctx);
			break;
		case GameStates.WIN:
			Win.render(ctx);
			break;
	}
}

function setKeyBindings() {
	window.addEventListener('keydown', function(e){
		keys[e.keyCode] = true;
		// Menu keys stay here because we only want it to react each key press
		switch(gameState) {
			case GameStates.MENU:
				Menu.onKeyDown(e);
				break;
			case GameStates.PLAYING:
				Player.onKeyDown(e);
				break;
			case GameStates.INSTRUCTIONS:
				Instructions.onKeyDown(e);
				break;
			case GameStates.DEATH:
				Death.onKeyDown(e);
				break;
			case GameStates.MODE:
				Mode.onKeyDown(e);
				break;
			case GameStates.OUT_OF_TIME:
				OutOfTime.onKeyDown(e);
				break;
			case GameStates.WIN:
				Win.onKeyDown(e);
				break;
		}
	});

	window.addEventListener('keyup', function(e){
		keys[e.keyCode] = false;

		switch(gameState) {
			case GameStates.PLAYING:
				Player.onKeyUp(e);
				break;
		}
	});

	document.getElementById('canvas').addEventListener('contextmenu', function(e) {
		if (e.button === 2) {
			e.preventDefault();
			return false;
		}
	}, false);
}

function searchSoundCloud(){
	var search = document.getElementById('searchSoundCloud').value;
	window.open("http://www.soundcloud.com/search?q=" + search);
}