package main

import (
	"bufio"
	"fmt"
	"image"
	"io"
	"log"
	"os"

	_ "image/png"
)

func convertMap(in io.Reader, out io.Writer) {
	i, _, err := image.Decode(in)
	if err != nil {
		log.Fatal(err)
	}
	b := i.Bounds()

	bout := bufio.NewWriter(out)

	fmt.Fprintln(bout, "/*THIS FILE WAS AUTOMATICALLY GENERATED*/")
	fmt.Fprintln(bout, "var LEVEL_DATA = [")

	for y := b.Min.Y; y < b.Max.Y; y++ {
		fmt.Fprint(bout, "[")
		for x := b.Min.X; x < b.Max.X; x++ {
			r, g, b, a := i.At(x, y).RGBA()

			t := -1

			if r == to16(0x00) && g == to16(0x00) && b == to16(0x00) {
				t = 0
			} else if r == to16(0x00) && g == to16(0xff) && b == to16(0x00) {
				// Top left floor tile
				t = 1
			} else if r == to16(0x00) && g == to16(0xae) && b == to16(0x00) {
				// Top floor tile
				t = 2
			} else if r == to16(0x00) && g == to16(0x6f) && b == to16(0x00) {
				// Top right  floor tile
				t = 3
			} else if r == to16(0xab) && g == to16(0x54) && b == to16(0x00) {
				// Middle left floor tile
				t = 4
			} else if r == to16(0x6b) && g == to16(0x35) && b == to16(0x00) {
				// Middle floor tile
				t = 5
			} else if r == to16(0x3c) && g == to16(0x1e) && b == to16(0x00) {
				// Middle right floor tile
				t = 6
			} else if r == to16(0xb2) && g == to16(0x15) && b == to16(0x00) {
				// Bottom left floor file
				t = 7
			} else if r == to16(0x71) && g == to16(0x0d) && b == to16(0x00) {
				// Bottom floor tile
				t = 8
			} else if r == to16(0x36) && g == to16(0x06) && b == to16(0x00) {
				// Bottom right floor tile
				t = 9
			} else if r == to16(0xb5) && g == to16(0xb5) && b == to16(0xb5) {
				// Spikes
				t = 10
			} else if r == to16(0x87) && g == to16(0x6e) && b == to16(0x07) {
				// Cliff bottom left
				t = 11
			} else if r == to16(0x5e) && g == to16(0x4c) && b == to16(0x05) {
				// Cliff bottom right
				t = 12
			}

			if t < 0 {
				log.Fatalf("Invalid tile at %d, %d: %d, %d, %d, %d", x, y, r, g, b, a)
			}

			fmt.Fprintf(bout, "%d,", t)
		}
		fmt.Fprintln(bout, "],")
	}

	fmt.Fprintln(bout, "];")

	if bout.Flush() != nil {
		log.Fatal(err)
	}
}

func to16(i uint32) uint32 {
	return (i << 8) + i
}

func main() {
	if len(os.Args) > 0 && (os.Args[1] == "-h" || os.Args[1] == "--help") {
		printHelp()
	} else {
		in := os.Stdin
		out := os.Stdout

		var err error = nil

		if len(os.Args) > 1 {
			in, err = os.Open(os.Args[1])
			if err != nil {
				log.Fatal(err)
			}
		}
		if len(os.Args) > 2 {
			out, err = os.Create(os.Args[2])
			if err != nil {
				log.Fatal(err)
			}
		}

		convertMap(in, out)
		in.Close()
		out.Close()
	}
}

func printHelp() {
	fmt.Printf("%s input output", os.Args[0])
}
